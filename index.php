<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet"/>
    <script type="text/javascript" src="main.js"></script>
    <title>Wira Test Phase 1</title>
</head>
<body>
    <div class="card">
        <div class="today">Today is <span class="textday"><?php echo date("l") ?></span></div>
            <div class="content">
                <?php 
                    $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
                    foreach ($days as $day) {
                        echo '<span onclick=selectedDay("' . $day . 
                        '") class="day' . ($day === date("l") ? ' currentday' : '') . '"' . 
                        ' id="d-' . $day .
                        '">' . $day . '</span>';
                    }
                ?>
            </div>
        <div id="daySelected" class="today">Selected Day is </div>
    </div>
</body>
</html>