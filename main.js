function selectedDay(day){
    let id = 'd-' + day;

    var dayElements = document.getElementsByClassName('day');
    
    for(i = 0; i < dayElements.length; i++){
        dayElements[i].classList.remove('selected');
    }

    var dayElement = document.getElementById(id);
    dayElement.classList.add("selected");

    document.getElementById('daySelected').innerHTML = "Selected Day is " + "<span>" + day + "</span>";
}